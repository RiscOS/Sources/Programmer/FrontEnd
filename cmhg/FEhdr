; Copyright 1999 Element 14 Ltd
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
;     http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.
;
; Title:   cmhg.FEHdr
; Purpose: module header for the generalised front end module
; Author:  IDJ
; History: 06-Apr-90: IDJ: created
;          21-May-90: IDJ: added frontend_setup
;          06-Jul-90: IDJ: added SWI FrontEnd_ExtendedCmdLine
;
;          Re-release
;          19-Nov-91: IDJ: version 1.14
;
;          Acorn C/C++:
;          24-Nov-94: IDJ: version 1.15
;          20-Dec-94: IDJ: version 1.16
;
#include "VersionNum"


module-is-runnable:                               ; module start code

initialisation-code:            FrontEnd_init
finalisation-code:              FrontEnd_final

service-call-handler:           FrontEnd_services  0x11; service-memory

title-string:                   FrontEnd

help-string: 		        DDE_FrontEnd  Module_MajorVersion_CMHG Module_MinorVersion_CMHG

date-string: 		        Module_Date_CMHG


command-keyword-table:          FrontEnd_commands
                                FrontEnd_Start(min-args: 4, max-args: 5, add-syntax:,
                                               help-text: "*FrontEnd_Start creates a new application by starting a new instance of the FrontEnd module.\r"
                                               invalid-syntax: "Syntax: *FrontEnd_Start -app <appname> -desc <filename> [input file]"),
                                FrontEnd_Setup(min-args: 10, max-args: 10, add-syntax:,
                                               help-text: "*FrontEnd_Setup runs a front end application as a child of another by starting a new instance of the FrontEnd module.\r"
                                               invalid-syntax: "Syntax: *FrontEnd_Setup -app <appname> -desc <filename> -task <task-id> -handle <handle> -toolflags <filename>")
